# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 17:16:07 2021
"""
import copy
import time

from ParserData import ParserData
from Solution import Solution

def insert_job_FI(solution, job_to_insert):
    """
    try inserting a job at each position
    if the job is already in the solution, it first removes the job
    stop at first improvement
    modify directly the sol object
    """
    
    pos = 0
    max_pos = len(solution.listed_jobs)
    new_sol = copy.deepcopy(solution)
    
    if job_to_insert in solution.listed_jobs:
        new_sol.listed_jobs.remove(job_to_insert)
        max_pos -= 1
        
    while pos <= max_pos:
        new_sol.listed_jobs.insert(pos, job_to_insert)
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > solution.getValue():
            # print("valid?",new_sol.isValid())
            # print("value",new_sol.getValue())
            return new_sol
        else:
            del new_sol.listed_jobs[pos]
        pos += 1
    return None

def insert_job_BI(solution, job_to_insert):
    max_pos = len(solution.listed_jobs)
    new_sol = copy.deepcopy(solution)
    best_sol = copy.deepcopy(solution)
    improved = False
    
    if job_to_insert in solution.listed_jobs:
        new_sol.listed_jobs.remove(job_to_insert)
        max_pos -= 1
        
    for pos in range(max_pos):
        new_sol.listed_jobs.insert(pos, job_to_insert)
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > best_sol.getValue():
            # print("valid?",new_sol.isValid())
            # print("value",new_sol.getValue())
            improved = True
            best_sol = copy.deepcopy(new_sol)
        
        del new_sol.listed_jobs[pos]
    
    if improved == True:
        return best_sol
    else:
        return None
    

def swap_job_FI(solution, job_to_swap):
    """
    try swapping a job with each job inside the solution
    job_to_swap can be any job (inside or not the actual sol)
    stop at first improvement
    modify directly the sol object
    """

    new_sol = copy.deepcopy(solution)

    if job_to_swap in new_sol.listed_jobs:
        pos1 = new_sol.listed_jobs.index(job_to_swap)
        intern_swap = True
    # in case we want to swap with a job not already in the sol
    # we add it at the end of the list of job
    # (we then remove the last job before evaluating the sol)
    else:
        intern_swap = False
        new_sol.listed_jobs.append(job_to_swap)
        pos1 = len(new_sol.listed_jobs)-1
    pos2 = 0
    while pos2 < len(new_sol.listed_jobs):
        new_sol.listed_jobs[pos1],new_sol.listed_jobs[pos2] = new_sol.listed_jobs[pos2],new_sol.listed_jobs[pos1]
        if intern_swap == False:
            save_job = new_sol.listed_jobs[-1]
            del new_sol.listed_jobs[-1]
            
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > solution.getValue():
            # print("valid?",new_sol.isValid())
            # print("value",new_sol.getValue())
            return new_sol
        else:
            if intern_swap == False:
                new_sol.listed_jobs.append(save_job)
            new_sol.listed_jobs[pos1],new_sol.listed_jobs[pos2] = new_sol.listed_jobs[pos2],new_sol.listed_jobs[pos1]
        pos2 += 1
    return None

def swap_job_BI(solution, job_to_swap):
    new_sol = copy.deepcopy(solution)
    best_sol = copy.deepcopy(solution)
    improved = False

    if job_to_swap in new_sol.listed_jobs:
        pos1 = new_sol.listed_jobs.index(job_to_swap)
        intern_swap = True
    # in case we want to swap with a job not already in the sol
    # we add it at the end of the list of job
    # (we then remove the last job before evaluating the sol)
    else:
        intern_swap = False
        new_sol.listed_jobs.append(job_to_swap)
        pos1 = len(new_sol.listed_jobs)-1

    for pos2 in range(len(new_sol.listed_jobs)):
        new_sol.listed_jobs[pos1],new_sol.listed_jobs[pos2] = new_sol.listed_jobs[pos2],new_sol.listed_jobs[pos1]
        if intern_swap == False:
            save_job = new_sol.listed_jobs[-1]
            del new_sol.listed_jobs[-1]
            
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > best_sol.getValue():
            # print("valid?",new_sol.isValid())
            # print("value",new_sol.getValue())
            improved = True
            best_sol = copy.deepcopy(new_sol)
        if intern_swap == False:
            new_sol.listed_jobs.append(save_job)
        new_sol.listed_jobs[pos1],new_sol.listed_jobs[pos2] = new_sol.listed_jobs[pos2],new_sol.listed_jobs[pos1]
        
    if improved == True:
        return best_sol
    else:
        return None
    
def insert_FI(solution):
    all_jobs = solution.instance.list_mandatory_jobs + solution.instance.list_jobs
    for index in range(len(all_jobs)):
        new_sol = insert_job_FI(solution,all_jobs[index])
        if new_sol is not None:
            return new_sol
    return None

def insert_BI(solution):
    best_sol = copy.deepcopy(solution)
    improved = False
    all_jobs = solution.instance.list_mandatory_jobs + solution.instance.list_jobs
    for index in range(len(all_jobs)):
        new_sol = insert_job_BI(solution,all_jobs[index])
        if new_sol is not None:
            if new_sol.getValue() > best_sol.getValue():
                best_sol = copy.deepcopy(new_sol)
                improved = True
    
    if improved == True:
        return best_sol
    else:
        return None
    
def swap_FI(solution):
    all_jobs = solution.instance.list_mandatory_jobs + solution.instance.list_jobs
    for index in range(len(all_jobs)):
        new_sol = swap_job_FI(solution,all_jobs[index])
        if new_sol is not None:
            return new_sol
    return None

def swap_BI(solution):
    best_sol = copy.deepcopy(solution)
    improved = False
    all_jobs = solution.instance.list_mandatory_jobs + solution.instance.list_jobs
    for index in range(len(all_jobs)):
        new_sol = swap_job_BI(solution,all_jobs[index])
        if new_sol is not None:
            if new_sol.getValue() > best_sol.getValue():
                best_sol = copy.deepcopy(new_sol)
                improved = True
    
    if improved == True:
        return best_sol
    else:
        return None

def inverse_FI(solution):
    """
    take 5 consecuting jobs and swap 1st and 5th, 2nd and 4th
    return the first improvement
    """
    new_sol = copy.deepcopy(solution)
    for index in range(len(solution.listed_jobs)-5):
        new_sol.listed_jobs[index],new_sol.listed_jobs[index+4] = new_sol.listed_jobs[index+4],new_sol.listed_jobs[index]
        
        new_sol.listed_jobs[index+1],new_sol.listed_jobs[index+3] = new_sol.listed_jobs[index+3],new_sol.listed_jobs[index+1]
        
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > solution.getValue():
            return new_sol
        
        new_sol.listed_jobs[index],new_sol.listed_jobs[index+4] = new_sol.listed_jobs[index+4],new_sol.listed_jobs[index]
    
        new_sol.listed_jobs[index+1],new_sol.listed_jobs[index+3] = new_sol.listed_jobs[index+3],new_sol.listed_jobs[index+1]

    return None

def inverse_BI(solution):
    """
    take 5 consecuting jobs and swap 1st and 5th, 2nd and 4th
    return the best improvement
    """
    new_sol = copy.deepcopy(solution)
    best_sol = copy.deepcopy(solution)
    improved = False
    
    for index in range(len(solution.listed_jobs)-5):
        new_sol.listed_jobs[index],new_sol.listed_jobs[index+4] = new_sol.listed_jobs[index+4],new_sol.listed_jobs[index]
        
        new_sol.listed_jobs[index+1],new_sol.listed_jobs[index+3] = new_sol.listed_jobs[index+3],new_sol.listed_jobs[index+1]
        
        new_sol.setValue()
        if new_sol.isValid() and new_sol.getValue() > best_sol.getValue():
            best_sol = copy.deepcopy(new_sol)
            improved = True
        
        new_sol.listed_jobs[index],new_sol.listed_jobs[index+4] = new_sol.listed_jobs[index+4],new_sol.listed_jobs[index]
    
        new_sol.listed_jobs[index+1],new_sol.listed_jobs[index+3] = new_sol.listed_jobs[index+3],new_sol.listed_jobs[index+1]

    if improved == True:
        return best_sol
    else:
        return None
    
def best_operator(solution, list_ope=[inverse_FI, inverse_BI, swap_FI, swap_BI, insert_FI, insert_BI]):
    """
    test on solution all operator of list_ope
    then return id of the best operator and corresponding solution
    id = 7 if no operator uprgades the solution
    """
    best_value = solution.getValue()
    best_idx = 7
    next_sol = copy.deepcopy(solution)
    for idx_ope in range(len(list_ope)):
        new_sol = list_ope[idx_ope](solution)
        if new_sol is not None:
            if new_sol.getValue() > best_value:
                best_value = new_sol.getValue()
                best_idx = idx_ope
                next_sol = copy.deepcopy(new_sol)
    return best_idx, next_sol
    
    
def algo_sans_ML(solution, parser = None):
    
    #Initialisation de l'opérateur actuel et du nombre d'opérateur.
    operateur = [inverse_FI, inverse_BI, swap_FI, swap_BI, insert_FI, insert_BI]
    operateur_actuel = 0
    nb_operateurs = 6
    
    #On parcourt l'ensemble des opérateurs.
    while operateur_actuel < nb_operateurs:
        solution_obtenu = operateur[operateur_actuel](solution)
        if solution_obtenu != None:
            if solution_obtenu.getValue() > solution.getValue():
                operateur_actuel = 1
                
                solution = solution_obtenu
                
                if parser is not None:
                    parser.store(solution_obtenu)
        else:
            operateur_actuel+=1
    return solution
    

        
if __name__ == "__main__": 
    parser = ParserData("../instances/data_40_15_80_1_ob.dat")
    inst = parser.parse()
    
    print("debut")
    solution = Solution(inst,[])
    solution.randomJobs()
    
    print(solution)
    print(solution.isValid())
    print(solution.isPrizeValid())
    print(solution.isTardinnessValid())
    print("tardinness = ", solution.tardinness)
    print("total prize = ", solution.total_prize)
    print("sol value = ",solution.getValue())
    
    print("###############################")
    

    #print("job a swap =", solution.discarded_jobs[-6].job_id)
    start = time.perf_counter()
    #improved_sol = swap_job_BI(solution,solution.discarded_jobs[-6])
    improved_sol = algo_sans_ML()
    end = time.perf_counter()
    if improved_sol is None:
        print("not improved")
    else:
        print(improved_sol)
        print(improved_sol.isValid())
        print(improved_sol.isPrizeValid())
        print(improved_sol.isTardinnessValid())
        print("tardinness = ", improved_sol.tardinness)
        print("total prize = ", improved_sol.total_prize)
        print("sol value = ",improved_sol.getValue())
        print(f"exec time : {end - start:0.4f} seconds")
    
    

    
        

    