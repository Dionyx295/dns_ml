This projetc is done by students in Computer Science, Polytech Tours. 
The goal is to see if Machine Learnig can improve result on shceduling problem.

The scheduling problem is based on this article : https://www.sjscience.org/files/papers/522/sjs_53fde96fcc4b4ce72d7739202324cd49.pdf
The dataset (description of jobs) can be found here https://optlab.di.unimi.it/PCSMSP/

Job.py, Instance.py and Solution.py are used to represent in memory data of the problem.
ParserSolution.py is used to read a dataset file and create an Instance object.
LocalSearch.py contains all functions used to define a solution (neighbour operator, init solution...)
NeuralNetwork.py defines the neural newtork, the associated dataset and does the training / evaluation.
ParserData.py is used to create and read data files that are used by the neural network.
IndicStat.py is used to compute statistics on differents resolution methods.

The base resolution method use a list of neighbour operators. It's a loop while one operator of the list can improve the solution.
The network is used to predict which operator will have the best improvement on the solution. 
There are two branches : nn_progress is the original version where we try to predict 7 operators, and two_operator, the latest vesrion, where we reduce the number of operator at two.
This reduction is due to the unbalanced repartition of operator in our dataset, we only kept operators that are relly often linked with best improvement.
