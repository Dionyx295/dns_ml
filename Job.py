# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 16:21:16 2021

@author: Jean-Malo
"""
class Job:
    def __init__(self, job_id, processing_time=0, due_date=0, deadline=0, prize=0):
        self.job_id = job_id
        self.processing_time = processing_time
        self.due_date = due_date
        self.deadline = deadline
        self.prize = prize
        
    def __eq__(self, job):
        if self.job_id == job.job_id:
            return True
        else:
            return False
        
    def __lt__(self, job):
        if self.prize == 0:
            self_ratio = self.processing_time + self.deadline
        else:
            self_ratio = (self.processing_time + self.deadline) / self.prize
        if job.prize == 0:
            job_ratio = job.processing_time + job.deadline
        else:
            job_ratio = (job.processing_time + job.deadline) / job.prize
        if self_ratio < job_ratio:
            return True
        else:
            return False
        
    def __str__(self):
        string = "\nJob_id = " + str(self.job_id)
        string += "\nProcessing time = " + str(self.processing_time)
        string += "\nDue date = " + str(self.due_date)
        string += "\nDeadline = " + str(self.deadline)
        string += "\nPrize = "+ str(self.prize)
        return string
        
    

        