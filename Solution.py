# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 17:08:07 2021
"""

from ParserData import ParserData
from Instance import Instance
import numpy as np



class Solution:
    #Constructeur de la classe Solution.
    #Prend en argument un tableau d'argument.
    def __init__(self, *args):
        #Si l'on a un seul argument, alors dans le cas ou l'argument 
        #est une instance, on initialise cette instance avec des valeurs par défaut.
        #Dans le cas où c'est une liste, alors on initialise les données à partir de la
        #liste obtenu à partir du parser de solution.
        if len(args) == 1:
            if isinstance(args[0], Instance):
                self.instance = args[0]
                self.listed_jobs = []
                self.discarded_jobs = []
                self.tardinness = 0
                self.total_prize = 0
            elif isinstance(args[0], list):
                row_csv = args[0]
                #print("row_csv : ", row_csv)
                parserData = ParserData(row_csv[0])
                self.best_op = int(row_csv[1])
                self.instance = parserData.parse()
                self.listed_jobs = []
                self.discarded_jobs = []
                for job_id in row_csv[2:]:
                    self.listed_jobs.append(self.instance.get_job(job_id))
                for job in self.instance.list_jobs:
                    if job not in self.listed_jobs:
                        self.discarded_jobs.append(job)
                self.setValue()
        #Si le nombre d'arguments est supérieur à 1 alors on test le nombre
        #d'arguments et on initialise en conséquences les attributs.
        elif len(args) > 1:
            if len(args) == 2:
                self.instance = args[0]
                self.listed_jobs = args[1]
                self.discarded_jobs = []
                self.tardinness = 0
                self.total_prize = 0
            if len(args) == 3:
                self.instance = args[0]
                self.listed_jobs = args[1]
                self.discarded_jobs = args[2]
                self.tardinness = 0
                self.total_prize = 0
            if len(args) == 4:
                self.instance = args[0]
                self.listed_jobs = args[1]
                self.discarded_jobs = args[2]
                self.tardinness = args[3]
                self.total_prize = 0
            if len(args) == 5:
                self.instance = args[0]
                self.listed_jobs = args[1]
                self.discarded_jobs = args[2]
                self.tardinness = args[3]
                self.total_prize = args [4]
                    
                    
        
    def isValid(self, print_error=False):
        """ checks if no deadline are crossed and mandatory jobs are done"""
        current_time = 0
        for job in self.listed_jobs:
            current_time += job.processing_time
            if current_time > job.deadline:
                if print_error:
                    print("deadline not respected")
                return False
            
        if self.instance.list_mandatory_jobs is not None:
            for job in self.instance.list_mandatory_jobs:
                if job not in self.listed_jobs:
                    if print_error:
                        print("mandatory jobs not respected")
                    return False
            
        return True
    
    def isPrizeValid(self):
        prize = 0
        for job in self.listed_jobs:
            prize += job.prize
        
        if prize == self.total_prize:
            return True
            
        return False
    
    def isTardinnessValid(self):
        current_time = 0
        tardinness = 0
        for job in self.listed_jobs:
            current_time += job.processing_time
            if current_time > job.due_date:
                tardinness += current_time - job.due_date
        
        if tardinness == self.tardinness:
            return True
            
        return False
    
    def setValue(self):
        current_time = 0
        tardinness = 0
        prize = 0
        for job in self.listed_jobs:
            prize += job.prize
            current_time += job.processing_time
            if current_time > job.due_date:
                tardinness += current_time - job.due_date
        
        self.tardinness = tardinness
        self.total_prize = prize
        
    def getValue(self):
        return self.total_prize - self.tardinness
    
    def setDiscardedJobs(self):
        self.discarded_jobs=[]
        for job in self.instance.list_jobs:
            if job not in self.listed_jobs:
                self.discarded_jobs.append(job)
    
    def randomJobs(self):
        """
        Shuffle all the jobs then browse the list only once, 
        add a job to the schedule if it respects the deadline
        """
        self.listed_jobs = []
        self.discarded_jobs = []
        jobs = np.array(self.instance.list_jobs)
        np.random.shuffle(jobs)
        
        current_time = 0
        # we start by doing mandatory jobs
        if self.instance.list_mandatory_jobs is not None:
            for job in self.instance.list_mandatory_jobs:
                self.listed_jobs.append(job)
                current_time += job.processing_time
                
                if current_time > job.due_date:
                    self.tardinness += current_time - job.due_date
                    
                self.total_prize += job.prize
                
        for job in jobs:
            # if jobs can be done
            if current_time + job.processing_time < job.deadline:  
                self.listed_jobs.append(job)
                current_time += job.processing_time
                
                if current_time > job.due_date:
                    self.tardinness += current_time - job.due_date
                self.total_prize += job.prize
            else:
                self.discarded_jobs.append(job)
                
                
            # if a job can't be done it will never be doable 
            # that's why we browse the list only once
            
    def init_determinist(self):
        """ to rework, only works when no mandtory jobs """
        self.listed_jobs = []

        sorted_jobs = sorted(self.instance.list_jobs)
        current_time = 0
        for job in sorted_jobs:
            if current_time + job.processing_time < job.deadline:
                self.listed_jobs.append(job)
                current_time += job.processing_time
                
            if current_time > job.due_date:
                self.tardinness += current_time - job.due_date
                self.total_prize += job.prize
                
                
                
    def detailled_string(self):
        string = ""
        for job in self.listed_jobs:
            string += str(job)
            string += ("\n*****************\n")
        return string
            
    def __str__(self):
        string = ""
        for job in self.listed_jobs:
            string += str(job.job_id)+" "
        return string
    
if __name__ == "__main__":       
    # some tests
    parser = ParserData("../instances/data_40_15_80_1.dat")
    inst = parser.parse()
    
    print("debut")
    solution = Solution(inst,[])
    #solution.randomJobs()
    solution.init_determinist()
    solution.setValue()
    print(solution.detailled_string())
    print(solution)
    #for job in solution.discarded_jobs:
    #    print(job.job_id)
    print(solution.isValid())
    print(solution.isPrizeValid())
    print(solution.isTardinnessValid())
    print("tardinness = ", solution.tardinness)
    print("total prize = ", solution.total_prize)
    
    #solutionParser = ParserSolution("test.txt")
    #solutionParser.store(solution)
    #list_sol_existante = solutionParser.parse()
    


