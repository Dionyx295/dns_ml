# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 16:26:04 2021
"""

class Instance:
    def __init__(self, list_jobs, list_mandatory_jobs=[], file_name = ""):
        self.file_name = file_name
        self.list_jobs = list_jobs
        self.list_mandatory_jobs = list_mandatory_jobs
        
    def get_job(self, id_job):
        for job in self.list_jobs:
            if job.job_id == int(id_job):
                return job
            
    def __str__(self):
        string="\nList jobs = ["
        for job in self.list_jobs:
            string += str(job)
            string += "\n*****************"
        string += "\n]"
        string += "\nList mandatory jobs = ["
        if(self.list_mandatory_jobs != None):
            for job in self.list_mandatory_jobs:
                string += str(job)
        string += "\n]"
        return string
            
        
        
        