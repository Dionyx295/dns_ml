# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 15:09:15 2021

@author: Hugo
"""

import csv
from Solution import Solution
import LocalSearch
from ParserData import ParserData

class ParserSolution:
    #Constructeur de la classe ParserSolution
    #Prend en argument file_path le chemin vers le fichier de stockage des solutions.
    def __init__(self, file_path):
        self.file_path = file_path
        
    #Écrit dans le fichier de chemin file_path, le chemin vers l'instance et les id de la solution.
    def store(self, solution):
        csv_row_list = [solution.instance.file_name]
        solution.best_op = LocalSearch.best_operator(solution)[0]
        csv_row_list.append(solution.best_op)
        for job in solution.listed_jobs:
            csv_row_list.append(job.job_id)
        try:
            with open(self.file_path, "a", newline='') as file:
                writer = csv.writer(file)
                
                writer.writerow(csv_row_list)
        except IOError:
            print("Erreur : Impossible d'accéder au fichier spécifier.")
            
    
    #On récupère l'ensemble des solutions stockés dans le fichier.
    def parse(self):
        #On initialise la liste des solutions qui sera retourné.
        list_solutions = []
        try:
            with open(self.file_path, "r") as file:
                #Récupération des données du fichier csv.
                reader = csv.reader(file)
                list_solutions = []
                for row in reader:                    
                    list_solutions.append(Solution(row))
                return list_solutions
        except IOError:
            print("Erreur : Impossible d'accéder au fichier spécifier.")
            
  
if __name__ == "__main__":       
    parser = ParserData("../instances/data_40_15_80_1.dat")
    inst = parser.parse()
    
    """
    print("debut")
    solution = Solution(inst,[])
    solution.randomJobs()
    # solution.init_determinist()
    print(solution.detailled_string())
    print(solution)

    #for job in solution.discarded_jobs:
    #    print(job.job_id)
    print(solution.isValid())
    print(solution.isPrizeValid())
    print(solution.isTardinnessValid())
    print("tardinness = ", solution.tardinness)
    print("total prize = ", solution.total_prize)
    
    solutionParser = ParserSolution("test.txt")
    solutionParser.store(solution)
    solution.randomJobs()
    solutionParser.store(solution)
    #list_sol_existante = solutionParser.parse()
    #"""
    #"""
    
    parserS = ParserSolution("solutionDataset_40.txt")
    solution = Solution(inst, [])
    
    for loop in range(100):
        print(loop)
        solution.randomJobs()
        solution.setValue()
        improved_sol = LocalSearch.algo_sans_ML(solution, parserS)
    #"""