# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 16:36:23 2021

@author: Jean-Malo
"""

from Instance import Instance
from Job import Job




def get_jobs(file_path):
    jobs = []
    print("file path : ",file_path)
    for i in range(10):
        job = Job(i,i*5,i*2,i*3,20-i)
        jobs.append(job)
    return jobs
    
class ParserData:
    #Constructeur de la classe ParserData.
    #Prend comme argument path le chemin vers le fichier à lire.
    def __init__(self, path):
        self.path = path
        
    #Méthode réalisant la lecture du fichier. 
    def parse(self):
        lines = []
        instance = Instance([], file_name = self.path)
        param = ""
        #On lit le fichier et on stocke chaque ligne dans lines.
        #Chaque lignes est découpé en liste de mots.
        try:
            with open(self.path, "r") as file:
                for line in file:
                    list_line = line.strip().split()
                    lines.append(list_line)
                
                
                    if list_line[0] == "set":
                        if list_line[1] == "J":
                            for value in list_line[3:]:
                                if ";" in value:
                                    value = value[:-1]
                                job = Job(int(value))
                                instance.list_jobs.append(job)
                        elif list_line[1] == "Jf":
                            for value in list_line[3:]:
                                if ";" in value:
                                    value = value[:-1]
                                job = instance.get_job(int(value))
                                instance.list_mandatory_jobs.append(job)
                        else:
                            print("Erreur : 'set J' ou 'set Jf' sont attendu comme valeur")
                    elif list_line[0] == "param":
                        param = list_line[1]
                    elif list_line[0] == ";":
                        #On a terminer une section on poursuit le traitement
                        continue
                    else:
                        if param == "p":
                            job = instance.get_job(int(list_line[0]))
                            job.processing_time = float(list_line[1])
                        elif param == "d":
                            job = instance.get_job(int(list_line[0]))
                            job.due_date = float(list_line[1])
                        elif param == "dbar":
                            job = instance.get_job(int(list_line[0]))
                            job.deadline = float(list_line[1])
                        elif param == "lambda":
                            job = instance.get_job(int(list_line[0]))
                            job.prize = float(list_line[1])
            return instance
        except IOError:
            print("Erreur : Impossible d'accéder au fichier spécifier.")


