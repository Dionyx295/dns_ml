# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 15:11:17 2021
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
import numpy as np

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from ParserSolution import ParserSolution

class FCNetwork(nn.Module):
    def __init__(self, nb_jobs, nb_ope=7):
        super(FCNetwork, self).__init__()
        
        self.input = nn.Linear(nb_jobs * 5,1000)
        
        self.hidden1 = nn.Linear(1000,1000)
        
        self.hidden2 = nn.Linear(1000,1000)
        
        self.output = nn.Linear(1000,nb_ope)
        
    def forward(self, x):
        x = self.input(x)
        x = F.relu(x)
        x = self.hidden1(x)
        x = F.relu(x)
        x = self.hidden2(x)
        x = F.relu(x)
        x = self.output(x)
        x = F.softmax(x, dim=0)
        
        return x


class SolutionDataset(Dataset):
    def __init__(self, file_path):
        self.solutions = [] # store solution descriptors
        self.operators = [] # store associated best operator
        
        parser = ParserSolution(file_path)
        list_solution = parser.parse()
        
        for sol in list_solution:
            sol.setDiscardedJobs()
            descriptor=[] # first binary list 
            for job in sol.listed_jobs:
                descriptor.append(1)
            for job in sol.discarded_jobs:
                descriptor.append(0)
            for job in sol.listed_jobs:
                descriptor.append(job.processing_time)
                descriptor.append(job.due_date)
                descriptor.append(job.deadline)
                descriptor.append(job.prize)
            for job in sol.discarded_jobs:
                descriptor.append(job.processing_time)
                descriptor.append(job.due_date)
                descriptor.append(job.deadline)
                descriptor.append(job.prize)
            op = [0] * 7
            op[sol.best_op - 1] = 1    
            
            self.solutions.append(descriptor)
            self.operators.append(op)
        
        # here we need to read file_path and fill the two list above
    
    def __len__(self):
        return len(self.operators)
    
    def __getitem__(self, idx):
        solution, operator = torch.Tensor(self.solutions[idx]).float(),torch.Tensor(self.operators[idx])
        return solution, operator
    
    
if __name__ == "__main__":
    net = FCNetwork(nb_jobs=40,nb_ope=7)
    loss_CE = torch.nn.CrossEntropyLoss(reduction='mean')
    opt = torch.optim.SGD(net.parameters(), lr=0.001)
    
    random_data = torch.from_numpy(np.random.random(200)).float()
    result = net(random_data)
    print(result)
    
    data = SolutionDataset(file_path="solutionDataset_40.txt")
    print(len(data))
    #print(data[2])
    
    
    dataloader = DataLoader(data, shuffle=True, batch_size=10)
    print(dataloader)
    loss_for_plot=[]
    size = len(dataloader.dataset)
    
    for epoch in range(1):
        print('epoch = ', epoch)
        net.train()
        trainLoss = 0
        for batch, (X, Y) in enumerate(dataloader):
            # Compute prediction and loss
            pred = net(X)
            print("pred :", pred)
            print("sum pred 0", sum(pred[0]))
            loss = loss_CE(pred, Y)
            print("Y :", Y)
    
            # Backpropagation
            opt.zero_grad()
            loss.backward()
            opt.step()
    
            if batch % 100 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")
        
                opt.step()
        
    plt.plot(loss_for_plot)
    plt.show()
    
    
    
        